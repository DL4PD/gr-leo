# Maintainer
* Kostis Triantafyllakis (kostastriantaf@gmail.com)

# Contributors
* Manolis Surligas (surligas@gmail.com)
* Electra Karakosta-Amarantidou (artkelh@gmail.com)
* kongr45gpen (electrovesta@gmail.com)